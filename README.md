# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_PORT` - Port of the app to forward requests to (default: `9000`)


# Prerequisites to setup the CICD:

## ECR Repositories:
 - recipe-app-api-proxy

## IAM:
 - recipe-app-api-proxy-ci (AWS-Setup/recipe-app-api-proxy-ci-policy.json)
